<p align="center">
    <h1 align="center">Stanbic Bank Dealer Portal</h1>
    <br>
</p>

Dealer Portal

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Install via Composer

Clone the project to the web root folder

Navigate to the project folder

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then update this project dependancies using the following command:

~~~
php composer.phar update all
~~~

Create the folder assets under web folder and ensure its writtable by the web user

~~~
mkdir web/assets
chmod -R 777 web/assets
~~~

Now you should be able to access the application through the following URL, assuming `dealerportal` is the directory
directly under the Web root.

~~~
http://localhost/dealerportal/web/
~~~


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=stanbic',
    'username' => 'root',
    'password' => 'dppassword',
    'charset' => 'utf8',
];
```

Run the following commands to initialise the database from the project directory

~~~
php yii migrate --migrationPath=@yii/rbac/migrations
php yii migrate --migrationPath=@bedezign/yii2/audit/migrations
php yii rbac/init
~~~



**NOTES:**
- Create additional tables using the db.sql script

- The application ships with a default user name and password please change these before continuing

~~~
Username : admin
Password : stanbic
~~~