<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RbacController
 *
 * @author KANAKE
 */
class RbacController extends Controller {
    
 public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $agentIndex = $auth->createPermission('agentIndex');
        $agentIndex->description = 'agent Index';
        $auth->add($agentIndex);

        $agentResetPin = $auth->createPermission('agentResetPin');
        $agentResetPin->description = 'Agent Reset Pin';
        $auth->add($agentResetPin);

        $userIndex = $auth->createPermission('userIndex');
        $userIndex->description = 'user Index';
        $auth->add($userIndex);

        $userView = $auth->createPermission('userView');
        $userView->description = 'user View';
        $auth->add($userView);

        $userCreate = $auth->createPermission('userCreate');
        $userCreate->description = 'user Create';
        $auth->add($userCreate);

        $userUpdate = $auth->createPermission('userUpdate');
        $userUpdate->description = 'user Update';
        $auth->add($userUpdate);

        $user = $auth->createPermission('user');
        $user->description = 'user';
        $auth->add($user);

        $userChangePassword = $auth->createPermission('userChangePassword');
        $userChangePassword->description = 'user Change Password';
        $auth->add($userChangePassword);

        $userResetAccount = $auth->createPermission('userResetAccount');
        $userResetAccount->description = 'user Reset Account';
        $auth->add($userResetAccount);

        $userAccount = $auth->createPermission('userAccount');
        $userAccount->description = 'user Account';
        $auth->add($userAccount);


        $accessLogIndex = $auth->createPermission('accessLogIndex');
        $accessLogIndex->description = 'access Log Index';
        $auth->add($accessLogIndex);


        $admin = $auth->createRole('admin');        $auth->add($admin);
        $auth->addChild($admin, $agentIndex);
        $auth->addChild($admin, $agentResetPin);
        $auth->addChild($admin, $userIndex);
        $auth->addChild($admin, $userView);
        $auth->addChild($admin, $userCreate);
        $auth->addChild($admin, $userUpdate);
        $auth->addChild($admin, $userChangePassword);
        $auth->addChild($admin, $userResetAccount);
        $auth->addChild($admin, $userAccount);
        $auth->addChild($admin, $user);
        $auth->addChild($admin, $accessLogIndex);

        $ccr = $auth->createRole('ccr');        
        $auth->add($ccr);
        $auth->addChild($ccr, $agentIndex);
        $auth->addChild($ccr, $agentResetPin);
        $auth->addChild($ccr, $userChangePassword);
        
        $auth->assign($admin, 1);
    }
}