<?php

$array = [
[
        'allow' => true,
        'actions' => ['index'],
        'roles' => ['agentIndex'],
],
[
        'allow' => true,
        'actions' => ['reset-pin'],
        'roles' => ['userIndex'],
], 
[
        'allow' => true,
        'actions' => ['index'],
        'roles' => ['userIndex'],
],
[
        'allow' => true,
        'actions' => ['view'],
        'roles' => ['userView'],
],
[
        'allow' => true,
        'actions' => ['create'],
        'roles' => ['userCreate'],
],
[
        'allow' => true,
        'actions' => ['update'],
        'roles' => ['userUpdate'],
],
[
        'allow' => true,
        'actions' => ['change-password'],
        'roles' => ['userChangePassword'],
],
[
        'allow' => true,
        'actions' => ['reset-account'],
        'roles' => ['userResetAccount'],
],
[
        'allow' => true,
        'actions' => ['account'],
        'roles' => ['userAccount'],
],
];

foreach ($array as $arr) {
        echo "        $" . $arr['roles'][0] .  " = " . "$" . "auth->createPermission('" . $arr['roles'][0] . "');\r\n";
        echo "        $" . $arr['roles'][0] . "->description = '" . implode(" ", preg_split('/(?=[A-Z])/',$arr['roles'][0])) . "';\r\n";
        echo "        $" . "auth->add($" . $arr['roles'][0]. ");\r\n";
    echo "\r\n";
}
echo "\r\n";
echo "        $" . "admin = $" . "auth->createRole('admin');";
echo "        $" . "auth->add($" . "admin);";
echo "\r\n";
foreach ($array as $arr) {
    echo "        $" . "auth->addChild($" . "admin, $" . $arr['roles'][0]. ");\r\n";
}