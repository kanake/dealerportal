<?php
namespace app\components\validators;

use yii\validators\Validator;

class MobileValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        if ($model->attribute != null || strlen($model->attribute) > 0) {
            break;
        } else {
            $this->addError($model, $attribute, 'Message is null');
        }
    }
}