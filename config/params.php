<?php

return [
    'adminEmail' => 'admin@example.com',
    'originator' => 'stanbic',
    'message' => 'Dear %s, Your account has been reset. Your one time PIN is %s',
];
