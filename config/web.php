<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'request' => [
            'cookieValidationKey' => 'idvCjris8ZlrQzorc7yNiZOjKkwK0y-e',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => '',
                'username' => '',
                'password' => '',
                'port' => '587',
                'encryption' => 'tls',
            ],
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        
        /*'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Disable index.php
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'enablePrettyUrl' => true,
            'rules' => array(
                    //'<controller:\w+>/<id:\d+>' => '<controller>/view',
                    //'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    //'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ],*/
        
    ],
    'params' => $params,
];
$config['modules']['gridview'] = [
         'class' => '\kartik\grid\Module'
    ];
$config['modules']['audit'] = [
    'class' => 'bedezign\yii2\audit\Audit',
    'layout' => 'main',
    'db' => 'db',
    'trackActions' => ['*'],
    'ignoreActions' => ['audit/*', 'debug/*'],
    'accessRoles' => ['admin'],
    'accessUsers' => [1],
    'compressData' => true,
    'panels' => [
        'audit/request',
        'audit/error',
        'audit/trail',
        'app/views' => [
            'class' => 'app\panels\ViewsPanel',
        ],
    ],
    'panelsMerge' => [
    ]
];
if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
