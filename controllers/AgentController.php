<?php

namespace app\controllers;

use Yii;
use app\models\Agent;
use app\models\AgentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\libraries\Encryption;
use yii\filters\AccessControl;
/**
 * AgentController implements the CRUD actions for Agent model.
 */
class AgentController extends Controller
{
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['agentIndex'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['reset-pin'],
                        'roles' => ['agentResetPin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Agent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AgentSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionResetPin($id) {
        $model = $this->findModel($id);
        $password = rand(1000, 9999);

            
        $model->salt = Encryption::generateSalt();
        $model->status = 3;
        if ($model->validate()) {
   
            $model->password = Encryption::encrypt($password, $model->salt);
            
            if ($model->save()) {
         
                 $sql = "INSERT INTO dbqueue (Originator, Destination, Message, SMSCID, MessageDirection) 
                            VALUES ('" . Yii::$app->params['originator'] . "', '" 
                            . $model->msisdn . "', '" . sprintf(Yii::$app->params['message'], $model->agent_name, $password) . "', 'SAFCOMKE', 'OUT')";
                \Yii::$app->db->createCommand($sql)->execute();
                Yii::$app->session->setFlash('info', 'Password has been changed. SMS has been sent to the agents number with their one time PIN');        
               
                
            }
        }
        $model->password = "";
        return $this->render('view', [
            'model' => $model,
        ]);
    }


    /**
     * Finds the Agent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Agent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
