<?php
namespace app\libraries;

/**
 * Description of CoreUtils
 *
 * @author user
 */
class CoreUtils {
    /**
     * 
     * @param type $userNumber
     * @return string
     */
    public static function checkMSISDN ($userNumber) {
        $numberRule[0] = array("netlen" => 2, "netNo" => 71, "prefix" => 254, "intprefix" => "+254", "localprefix" => "0", "userlen" => 7, "numlen" => 12); //safaricom 1
        $numberRule[1] = array("netlen" => 2, "netNo" => 72, "prefix" => 254, "intprefix" => "+254", "localprefix" => "0", "userlen" => 7, "numlen" => 12); //safaricom 1
        $numberRule[2] = array("netlen" => 2, "netNo" => 73, "prefix" => 254, "intprefix" => "+254", "localprefix" => "0", "userlen" => 7, "numlen" => 12); //celtel
        $numberRule[3] = array("netlen" => 2, "netNo" => 75, "prefix" => 254, "intprefix" => "+254", "localprefix" => "0", "userlen" => 7, "numlen" => 12); //Yu
        $numberRule[4] = array("netlen" => 2, "netNo" => 77, "prefix" => 254, "intprefix" => "+254", "localprefix" => "0", "userlen" => 7, "numlen" => 12); //Orange
        $numberRule[6] = array("netlen" => 2, "netNo" => 78, "prefix" => 254, "intprefix" => "+254", "localprefix" => "0", "userlen" => 7, "numlen" => 12); //celtel
        $numberRule[7] = array("netlen" => 2, "netNo" => 70, "prefix" => 254, "intprefix" => "+254", "localprefix" => "0", "userlen" => 7, "numlen" => 12); // safaricom
        $numberRule[8] = array("netlen" => 2, "netNo" => 79, "prefix" => 254, "intprefix" => "+254", "localprefix" => "0", "userlen" => 7, "numlen" => 12); // safaricom
        
        $userNumber = 0 + str_replace(' ', '', $userNumber);
        $len = strlen($userNumber);
        $mobileNumber = 0;
        $netNo = 0;
        $myRule = 0;
         if ($userNumber > 700000000 and $userNumber < 254799999999) {
              foreach ($numberRule as $rule) {
                    $upperNet = 0;
                    $netOK = true;
                    $xs = $rule["userlen"] + $rule["netlen"];
                    if ($len < $xs) {
                        $descr = "INVALID msisdn: $userNumber too short";
                        continue;
                    }
                    $mobileNumber = 0 + substr($userNumber, (-1 * $xs), $xs);
                    $netNo = 0 + substr($userNumber, (-1 * $xs), $rule["netlen"]);

                    if ($len >= $xs) {
                        $netOK = false;
                        $lx = $len - $xs;
                        if ($lx > 0) {
                            $upperNet = 0 + substr($userNumber, (-1 * ($lx + $xs)), $lx);
                        } else {
                            $upperNet = 0;
                        }

                        if ($upperNet == 0 or $upperNet == $rule["prefix"] or $upperNet == $rule["netNo"]
                            or $upperNet == $rule["localprefix"] or $upperNet == $rule["intprefix"]) {
                            $netOK = true;
                        }
                    }

                    if ($netNo == $rule["netNo"] and $netOK) {
                        $myRule = $rule;
                        break;
                    }
              }
              if (is_null($myRule)) {
                $descr = "INVALID msisdn: $userNumber not a valid network";
                $number = "0";
              }
              $number = $myRule["prefix"] . $mobileNumber;
              return $number;  
                
         } else {
             $number = "0";
        } 
    }
    /**
     * 
     * @param type $amount
     * @return boolean
     */
    public static function verifyAmount ($amount){
        if (preg_match('#^[0-9]+(\.[0-9]{0,2})?$#', $amount)){
          $money = number_format($amount, 2);
          return true;
        } else { 
          return false;
        }
    }
    
    public static function getSMSC ($msisdn) {
        $prefix  = substr($msisdn, 0, 5);
        switch ($prefix) {
            case "25470":
            case "25471":
            case "25472":
            case "25479":
                $smsc = "SAFARICOM_KE";
            break;
            case "25473":
            case "25478":
            case "25475":
                $smsc = "AIRTEL_KE";
            break;    
            case "25477":
                $smsc = "ORANGE_KE";
            break;     
        }
        return $smsc;
    }
   

}

?>
