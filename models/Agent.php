<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "agent".
 *
 * @property int $id
 * @property string $agent_code
 * @property string $msisdn
 * @property string $agent_name
 * @property int $status
 * @property string $password
 * @property string $salt
 * @property int $password_tries
 * @property string $date_created
 * @property int $created_by
 * @property string $date_modified
 */
class Agent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agent';
    }


    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_created', 'date_modified'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'date_modified',
                ],
                'value' => new Expression('NOW()'),
            ],
             'bedezign\yii2\audit\AuditTrailBehavior'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'password_tries', 'created_by'], 'integer'],
            [['password', 'salt'], 'string'],
            [['date_created', 'date_modified'], 'safe'],
            [['agent_code'], 'string', 'max' => 20],
            [['msisdn'], 'string', 'max' => 12],
            [['agent_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agent_code' => 'Agent Code',
            'msisdn' => 'Msisdn',
            'agent_name' => 'Agent Name',
            'status' => 'Status',
            'password' => 'Password',
            'salt' => 'Salt',
            'password_tries' => 'Password Tries',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'date_modified' => 'Date Modified',
        ];
    }
}
