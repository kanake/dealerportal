<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Agent;
use app\libraries\CoreUtils;

/**
 * AgentSearch represents the model behind the search form of `app\models\Agent`.
 */
class AgentSearch extends Agent
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'password_tries', 'created_by'], 'integer'],
            [['agent_code', 'msisdn', 'agent_name', 'password', 'salt', 'date_created', 'date_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Agent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (CoreUtils::checkMSISDN($this->msisdn) <= 0) {
            $query->where(['msisdn' => ""]);
        }


        $query->andFilterWhere(['like', 'agent_code', $this->agent_code])
            ->andFilterWhere(['like', 'msisdn', $this->msisdn]);

        return $dataProvider;
    }
}
