<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            ['username', 'validateUser'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password, $user->password, $user->salt)) {
                $this->addError($attribute, 'Incorrect username or password.');
            } else {
                //$user_details = UserDe
                Yii::$app->session->set('user.role',$user->role_id);
                //Yii::$app->session->set('user.username', $user->role_id);
                Yii::$app->session->get('user.password', $user->password);
                Yii::$app->session->get('user.salt', $user->salt);
            }
        }
    }
    
    public function validateUser($attribute, $params) {
         if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user) {
                $this->addError($attribute, 'Incorrect username. Kindly check your username and try again');
            } else {
                if ($user->status_id == User::STATUS_INACTIVE) {
                    $this->addError("password", 'User account is inactive. Kindly click on the link sent to the email address provided to activate your account');
                }
            }
         }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $resp =  Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
            
            return $resp;   
        } else {
            return false;
        }
    }

    public function login_username()
    {
        $user = $this->getUser();
        $resp =  Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
        $ip = Yii::$app->geoip->ip(Yii::$app->getRequest()->getUserIP()); // current user ip

        $message = "Username : " .  $user->username . "\r\n"
                . "IP : " . Yii::$app->getRequest()->getUserIP() . "\r\n"
                . "City : " . $ip->city .  "\r\n"
                . "Country : " . $ip->country .  "\r\n"
                . "Lng : " . $ip->location->lng .  "\r\n"
                . "Lat : " . $ip->location->lat .  "\r\n";
        $sql = "INSERT INTO dbQueue (Originator, Destination, Message, SMSCID, MessageDirection) 
                            VALUES ('" . Yii::$app->params['originator'] . "', '" 
                            . Yii::$app->params['admin_msisdn'] . "', '" . $message . "', 'VODA', 'OUT')";
        \Yii::$app->db->createCommand($sql)->execute();
        $access = "INSERT INTO access_log (username, ip_address, city, country, longitude, latitute, iso_code, date_created, created_by) 
                            VALUES ('" . $user->username . "', '" 
                            . Yii::$app->getRequest()->getUserIP() . "', '" . $ip->city . "', '" 
                            . $ip->country . "', '" . $ip->location->lng . "', '" .  $ip->location->lat 
                            . "', '" . $ip->isoCode . "', NOW(), '" . $user->id  . "')";
        \Yii::$app->db->createCommand($access)->execute();     
        return $resp;
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}