<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "role".
 *
 * @property string $id
 * @property string $role
 * @property string $description
 * @property string $dateCreated
 * @property integer $createdBy
 * @property string $dateModified
 * @property integer $modifiedBy
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role', 'description'], 'required'],
            [['dateCreated', 'dateModified'], 'safe'],
            [['createdBy', 'modifiedBy'], 'integer'],
            [['role'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 100]
        ];
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['dateCreated', 'dateModified'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'dateModified',
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role' => 'Role',
            'description' => 'Description',
            'dateCreated' => 'Date Created',
            'createdBy' => 'Created By',
            'dateModified' => 'Date Modified',
            'modifiedBy' => 'Modified By',
        ];
    }
    public function getCreatedBy() {
        $user = User::findOne($this->createdBy);
        return $user ? $user->username : "";
    }
}
