<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $statusName
 * @property string $statusDesc
 * @property string $dateCreated
 * @property integer $createdBy
 * @property string $dateModified
 * @property integer $modifiedBy
 *
 * @property Participant[] $participants
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['dateCreated', 'dateModified'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'dateModified',
                ],
                'value' => new Expression('NOW()'),
            ],
            'bedezign\yii2\audit\AuditTrailBehavior'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statusName', 'statusDesc'], 'required'],
            [['dateCreated', 'dateModified'], 'safe'],
            [['createdBy', 'modifiedBy'], 'integer'],
            [['statusName'], 'string', 'max' => 20],
            [['statusDesc'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'statusName' => 'Status Name',
            'statusDesc' => 'Status Desc',
            'dateCreated' => 'Date Created',
            'createdBy' => 'Created By',
            'dateModified' => 'Date Modified',
            'modifiedBy' => 'Modified By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getusername() {
        $user = user::findOne($this->createdBy);
        //$training = Training::find()->where(['id' => $this->trainingId])->one();
        return isset($user) ? $user->username : "";
    }
    public function getusernameM() {
        $user = user::findOne($this->createdBy);
        //$training = Training::find()->where(['id' => $this->trainingId])->one();
        return isset($user) ? $user->username : "";
    }
    public function getParticipants()
    {
        return $this->hasMany(Participant::className(), ['statusId' => 'id']);
    }
}
