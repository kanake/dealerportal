<?php
 
namespace app\models;
 
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
 
/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'role_id', 'status_id', 'created_by', 'modified_by'], 'integer'],
            [['username', 'firstname', 'lastname', 'msisdn', 'email', 'password', 'salt', 'activation_key', 'date_created', 'date_modified'], 'safe'],
        ];
    }
 
    /**
     * @inheritdoc
     */
    public function scenarios()
    { // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
 
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();
 
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
 
        $this->load($params);
 
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
 
        /*$query->andFilterWhere([
            'id' => $this->id,
            'role_id' => $this->role_id,
            'statusId' => $this->statusId,
            
        ]);*/
 
        
        return $dataProvider;
    }
}
 

