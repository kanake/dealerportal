<?php

use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\DateTimePicker;
use kartik\helpers\Html;
use kartik\builder\Form;


/* @var $this yii\web\View */
/* @var $model app\models\ScheduleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL, 'method' => 'get', 'fullSpan' => 11, 'formConfig'=>['labelSpan'=>4,  'deviceSize' => ActiveForm::SIZE_SMALL]]);
echo Form::widget([ // fields with labels
    'model'=>$model,
    'form'=>$form,
    'columns'=>2,
    'attributes'=>[
        'msisdn'=>[
            'label'=>'Mobile No.', 
            'options'=>['placeholder'=>'Mobile No.'],
            'type' => Form::INPUT_TEXT

        ],
        /*'name'=>['label'=>'Customer Name', 
            'options'=>['placeholder'=>'Customer Name'],
            'type' => Form::INPUT_TEXT],
        */
        
        
       
    ]
]);

echo '<div class="text-right">' . Html::submitButton('Search', ['class'=>'btn btn-default']) . '</div>';


ActiveForm::end();

?>
