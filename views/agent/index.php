<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use \yiister\gentelella\widgets\Panel;
use kartik\export\ExportMenu;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12 col-xs-12">
<?php Panel::begin(['header' => "Agent Information",]);?>

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'agent_code',
            'msisdn',
            'agent_name',
            'status',
            //'password:ntext',
            //'salt:ntext',
            //'password_tries',
            'date_created',
            'created_by',
            'date_modified',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{reset}',
                'buttons' => [
                    'reset' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, [
                                    'title' => Yii::t('app', 'Reset PIN'),                            
                        ]);
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'reset') {
                        return  Url::to(['agent/reset-pin', 'id' => $model['id']]);
                    }
                }
            ],
        ],
    ]); ?>

 <?php Panel::end() ?>
</div>
</div>
