<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \yiister\gentelella\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model app\models\Agent */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Agents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agent-view">

    <?php
        Panel::begin(['header' => "Agent ID : " . $this->title,]);?>

    <?= \yiister\gentelella\widgets\FlashAlert::widget(['showHeader' => true]) ?>    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'agent_code',
            'msisdn',
            'agent_name',
            //'status',
            //'password:ntext',
            //'salt:ntext',
            //'password_tries',
            'date_created',
            //'created_by',
            //'date_modified',
        ],
    ]) ?>


 <?php Panel::end();
?>
</div>
