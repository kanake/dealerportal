<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$bundle = yiister\gentelella\assets\Asset::register($this);


?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="nav-<?= !empty($_COOKIE['menuIsCollapsed']) && $_COOKIE['menuIsCollapsed'] == 'true' ? 'sm' : 'md' ?>" >
<?php $this->beginBody(); ?>
<div class="container body">

    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="/" class="site_title"><i class="fa fa-paw"></i> <span>Betamasano Portal!</span></a>
                </div>
                <div class="clearfix"></div>
                <?php Yii::$app->user->isGuest ?"":
                    '<div class="profile">
                        <div class="profile_pic">
                            <img src="http://placehold.it/128x128" alt="..." class="img-circle profile_img">
                        </div>

                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2>' . Yii::$app->user->identity->username .'</h2>
                        </div>
                    </div>'
                    ?>
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>Betamasano</h3>
                        <?=
                        \yiister\gentelella\widgets\Menu::widget(
                            [
                                "items" => [
                                    
                                    [
                                        "label" => "Agents",
                                        "url" => ["agent/index"],
                                        "icon" => "cog",
                                        'visible' => (!Yii::$app->user->isGuest)?\Yii::$app->user->can('agentIndex'):0
                                       
                                    ],
                                    [
                                        "label" => "Users",
                                        "url" => "#",
                                        "icon" => "user",
                                        "items" => [
                                            [
                                                "label" => "All Users",
                                                "url" => ["user/index"],
                                                'visible' => (!Yii::$app->user->isGuest)?\Yii::$app->user->can('userIndex'):0
                                            ],
                                            [
                                                "label" => "New User",
                                                "url" => ["user/create"],
                                                'visible' => (!Yii::$app->user->isGuest)?\Yii::$app->user->can('userCreate'):0
                                            ],
                                            [
                                                "label" => "Change Password",
                                                "url" => ["user/change-password"],
                                                'visible' => (!Yii::$app->user->isGuest)?\Yii::$app->user->can('userChangePassword'):0
                                            ],
                                           
                                        ],
                                        'visible' => (!Yii::$app->user->isGuest)?\Yii::$app->user->can('user'):0
                                    ],
                                    [
                                        "label" => "Audit Trails",
                                        "url" => ["audit"],
                                        "icon" => "info",
                                        'visible' => (!Yii::$app->user->isGuest)?\Yii::$app->user->can('accessLogIndex'):0
                                    ],
                                ],
                            ]
                        );
                        ?>
                    </div>

                </div>
                <!-- /sidebar menu -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            
        <?php
            NavBar::begin([
                    //'brandLabel' => '', 
                    'options' => [
                        'class' => 'nav_menu',
                    ],
                ]);
                echo '<div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>';
                echo Nav::widget([
                    'options' => ['class' => 'nav navbar-nav navbar-right'],
                    'items' => [
                       Yii::$app->user->isGuest ?
                            ['label' => 'Login', 'url' => ['/site/login']] :
                            [
                                'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                                'url' => ['/site/logout'],
                                'linkOptions' => ['data-method' => 'post']
                            ],




                    ],



                    //'options' => ['class' => 'navbar-nav'],
                ]);
                NavBar::end();
            ?>
            
        </div>



        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <?php if (isset($this->params['h1'])): ?>
                <div class="page-title">
                    <div class="title_left">
                        <h1><?= $this->params['h1'] ?></h1>
                    </div>
                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="clearfix"></div>

            <?= $content ?>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
            <div class="pull-right">
                <a href="https://"" rel="nofollow" target="_blank">Betamasano</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<!-- /footer content -->
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>