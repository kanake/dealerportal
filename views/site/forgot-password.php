<?php
use kartik\helpers\Html;
use kartik\builder\Form;
use kartik\widgets\ActiveForm;
use \yiister\gentelella\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-md-12 col-xs-12">

<?php
Panel::begin(['header' => "Forgot Password",]);?>
<?= \yiister\gentelella\widgets\FlashAlert::widget(['showHeader' => true]) ?>
<?php
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL, 'fullSpan' => 10, 'formConfig'=>['labelSpan'=>2,  'deviceSize' => ActiveForm::SIZE_SMALL]]);
echo  Form::widget([
   'model'=>$model,
    'form'=>$form,
    'columns'=>1,
     'attributeDefaults'=>[
        'type'=>Form::INPUT_TEXT,
        'labelOptions'=>['class'=>'col-md-3'], 
        'inputContainer'=>['class'=>'col-md-9'], 
        'container'=>['class'=>'form-group'],
    ],
    'attributes'=>[
        
        'email_address' => [
            'label' => 'Email',
            'type' => Form::INPUT_TEXT, 
            'options' => ['placeholder' => 'Email Address e.g 123@me.co.ke'],
            //'hint' => 'Email Address '
        ],
        
       'actions'=>[
            'type'=>Form::INPUT_RAW, 
            'value'=>'<div style="text-align: right; margin-top: 20px">' . 
                Html::submitButton('Email New Password', ['class'=>'btn btn-primary']) . 
                '</div>'
        ],
    ]
]);
ActiveForm::end();
 Panel::end();
?>
</<div id="system-messages" style="opacity: 1; display: none"></div>
</div>
