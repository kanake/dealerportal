<?php

use kartik\helpers\Html;
use kartik\builder\Form;
use kartik\widgets\ActiveForm;
use \yiister\gentelella\widgets\Panel;
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-12 col-xs-12">

<?php
Panel::begin(['header' => "Change Password",]);?>
<div id="system-messages" style="opacity: 1; display: none"></div>
<?php

<?php
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL, 'fullSpan' => 10, 'formConfig'=>['labelSpan'=>2,  'deviceSize' => ActiveForm::SIZE_SMALL]]);
$content =  Form::widget([
   'model'=>$model,
    'form'=>$form,
    'columns'=>1,
     'attributeDefaults'=>[
        'type'=>Form::INPUT_TEXT,
        'labelOptions'=>['class'=>'col-md-3'], 
        'inputContainer'=>['class'=>'col-md-9'], 
        'container'=>['class'=>'form-group'],
    ],
    'attributes'=>[
        'currentPassword' => [
            'type' => Form::INPUT_PASSWORD , 
            'options' => ['placeholder' => 'Enter Cuurent Password']
        ],
        'password' => [
            'type' => Form::INPUT_PASSWORD , 
            'options' => ['placeholder' => 'Enter New Password']
        ],
        'repeatPassword' => [
            'type' => Form::INPUT_PASSWORD , 
            'options' => ['placeholder' => 'Confirm Password']
        ],
        'actions'=>[
            'type'=>Form::INPUT_RAW, 
            'value'=>'<div style="text-align: right; margin-top: 20px">' . 
                Html::submitButton('Change Password', ['class'=>'btn btn-primary']) . 
                '</div>'
        ],
    ]
]);
ActiveForm::end();
 Panel::end();
?>
</<div id="system-messages" style="opacity: 1; display: none"></div>
</div>


