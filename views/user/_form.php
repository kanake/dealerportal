<?php

use kartik\helpers\Html;
use kartik\builder\Form;
//use yii\widgets\ActiveForm;
use kartik\widgets\ActiveForm;
use \yiister\gentelella\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-md-12 col-xs-12">

<?php
Panel::begin(['header' => "New User",]);
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL, 'fullSpan' => 10, 'formConfig'=>['labelSpan'=>2,  'deviceSize' => ActiveForm::SIZE_SMALL]]);
echo  Form::widget([
   'model'=>$model,
    'form'=>$form,
    'columns'=>1,
     'attributeDefaults'=>[
        'type'=>Form::INPUT_TEXT,
        'labelOptions'=>['class'=>'col-md-3'], 
        'inputContainer'=>['class'=>'col-md-9'], 
        'container'=>['class'=>'form-group'],
    ],
    'attributes'=>[
        'role_id' => [
            'type' => Form::INPUT_DROPDOWN_LIST, 
            'items'=>$model->getRoleList(),
            'options' => ['prompt' => 'Select Role ..']
        ],
        'firstname' => [
            'type' => Form::INPUT_TEXT, 
            'options' => ['placeholder' => 'First Name']
        ],
        'lastname' => [
            'type' => Form::INPUT_TEXT, 
            'options' => ['placeholder' => 'Last Name']
        ],
        'username' => [
            'type' => Form::INPUT_TEXT, 
            'options' => ['placeholder' => 'Username']
        ],
        'password' => [
            'type' => Form::INPUT_PASSWORD , 
            'options' => ['placeholder' => 'Enter Password']
        ],
        'repeat_password' => [
            'type' => Form::INPUT_PASSWORD , 
            'options' => ['placeholder' => 'Confirm Password']
        ],
        'email' => [
            'type' => Form::INPUT_TEXT, 
            'options' => ['placeholder' => 'Email Address e.g 123@me.co.ke']
        ],
        'msisdn' => [
            'type' => Form::INPUT_TEXT, 
            'options' => ['placeholder' => 'Mobile No.']
        ],
       'actions'=>[
            'type'=>Form::INPUT_RAW, 
            'value'=>'<div style="text-align: right; margin-top: 20px">' . 
                Html::submitButton('Create', ['class'=>'btn btn-primary']) . 
                '</div>'
        ],
    ]
]);
ActiveForm::end();
 Panel::end();
?>
</div>
</div>
