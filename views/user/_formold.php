<?php

use kartik\helpers\Html;
use kartik\builder\Form;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL, 'fullSpan' => 10, 'formConfig'=>['labelSpan'=>2,  'deviceSize' => ActiveForm::SIZE_SMALL]]);

$content =  Form::widget([
   'model'=>$model,
    'form'=>$form,
    'columns'=>1,
     'attributeDefaults'=>[
        'type'=>Form::INPUT_TEXT,
        'labelOptions'=>['class'=>'col-md-3'], 
        'inputContainer'=>['class'=>'col-md-9'], 
        'container'=>['class'=>'form-group'],
    ],
    'attributes'=>[
        
        'first_name' => [
            'type' => Form::INPUT_TEXT, 
            'options' => ['placeholder' => 'First Name']
        ],
        'last_name' => [
            'type' => Form::INPUT_TEXT, 
            'options' => ['placeholder' => 'Last Name']
        ],
        'username' => [
            'type' => Form::INPUT_TEXT, 
            'options' => ['placeholder' => 'Username']
        ],
        'password' => [
            'type' => Form::INPUT_PASSWORD , 
            'options' => ['placeholder' => 'Enter Password']
        ],
        'email' => [
            'type' => Form::INPUT_TEXT, 
            'options' => ['placeholder' => 'Email Address e.g 123@me.co.ke']
        ],
       'actions'=>[
            'type'=>Form::INPUT_RAW, 
            'value'=>'<div style="text-align: right; margin-top: 20px">' . 
                Html::submitButton('Create', ['class'=>'btn btn-primary']) . 
                '</div>'
        ],
        'actions'=>[
            'type'=>Form::INPUT_RAW, 
            'value'=>'<div style="text-align: right; margin-top: 20px">' . 
                Html::resetButton('Reset', ['class'=>'btn btn-default']) . ' ' .
                Html::submitButton('Update', ['type'=>'button', 'class'=>'btn btn-primary']) . 
                '</div>'
        ],
    ]
]);

echo Html::panel(
    ['heading' => $this->title, 'body' => '<div class="panel-body">' . $content . '</div>'],
    Html::TYPE_PRIMARY
);

echo Html::endForm();
ActiveForm::end();

