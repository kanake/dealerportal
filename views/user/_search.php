<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\models\UserSearchSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
                'formConfig' => ['labelSpan' => 4, 'deviceSize' => ActiveForm::SIZE_SMALL],
                'type' => ActiveForm::TYPE_HORIZONTAL,
    ]);
    ?>
    
    <div class="form-group kv-fieldset-inline">

        <?=
        Html::activeLabel($model, 'roleId', [
            'label' => 'Role',
            'class' => 'col-sm-3 control-label'
        ])
        ?>
        <div class="col-sm-3">     
            <?= $form->field($model, 'roleId', ['showLabels' => false])->dropDownList($model->getRoleList(),  ['prompt'=>'Select Role']) ?>
        </div>  
     </div>
    <div class="form-group kv-fieldset-inline">
        <?=
        Html::activeLabel($model, 'username', [
            'label' => 'Username',
            'class' => 'col-sm-3 control-label'
        ])
        ?>
         <div class="col-sm-3">
            <?= $form->field($model, 'username', ['showLabels' => false]) ?>
        </div>  
        <?=
        Html::activeLabel($model, 'email', [
            'label' => 'Email',
            'class' => 'col-sm-3 control-label'
        ])
        ?>
        <div class="col-sm-3">     
            <?= $form->field($model, 'email', ['showLabels' => false]) ?>
        </div>  
     </div>

    <?php // $form->field($model, 'id') ?>

    <?php // $form->field($model, 'clientId') ?>

    <?php // $form->field($model, 'roleId') ?>

    <?php // $form->field($model, 'username') ?>

    <?php // $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'msisdn') ?>

    <?php // echo $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'salt') ?>

    <?php // echo $form->field($model, 'activationKey') ?>

    <?php // echo $form->field($model, 'status_id') ?>

    <?php // echo $form->field($model, 'dateCreated') ?>

    <?php // echo $form->field($model, 'createdBy') ?>

    <?php // echo $form->field($model, 'dateModified') ?>

    <?php // echo $form->field($model, 'modifiedBy') ?>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-10">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?php //Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        </div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
