<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use \yiister\gentelella\widgets\Panel;

$this->title = 'Registration Complete';

 \yiister\gentelella\widgets\FlashAlert::widget(['showHeader' => true]);

 ?>

<div class="row">
    <div class="col-md-12 col-xs-12">
        <?php
        Panel::begin();
        Panel::begin(['header' => 'Personal Details',]);
        echo DetailView::widget([
            'model' => $individual,
            'attributes' => [
                'salutationName',
                'first_name',
                'last_name',
                'genderValue',
                'date_of_birth',
                'id_number',
                'phone_number',
                'volunteer',
                'wardName',
                [
                    'attribute'=>'curriculum_vitae',
                    'format'=>'raw',
                    'value'=>Html::a($individual->curriculum_vitae, ['user/download', 'filename' => $individual->curriculum_vitae]),
                ],
            ]
        ]);
        Panel::end();

        foreach ($education as $edu) {

            Panel::begin(['header' => 'Education Details',]);
            echo DetailView::widget([
                'model' => $edu,
                'attributes' => [
                    'DegreeLevelName',
                    'courseName',
                ]
            ]);
            Panel::end();
        }

        foreach ($qualification as $qua) {

            Panel::begin(['header' => 'Qualification Details']);
            echo DetailView::widget([
                'model' => $qua,
                'attributes' => [
                    //'sectorName',
                    'organisation_name',
                    'job_title',
                    'years_experience',
                ]
            ]);
            Panel::end();
        }

/*[
                'attribute'=>'upload',
                'format'=>'raw',
                'value'=>Html::a($project->upload, ['project/download', 'fileName' => $project->upload]),
            ],*/
        Panel::end();
        ?>
</div>
</div>