<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\detail\DetailView;
use yii\db\Query;
use yii\jui\Accordion;
use yii\bootstrap\Collapse;
use yii\bootstrap\Dropdown;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'CRM';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-about">
    <div class=row">
      
        <div class="col-lg-4">
        <h3>BETTING WITH BETA MASANO</h3>
            <div class="table-responsive ">
                <table class="table  borderless table-hover">
                    <tbody>
                        <tr>
                            <?php
                            $groupQ = new Query;
                            $groupQ->from('Help')
                                    ->groupBy('category');
                            $groupD = $groupQ->all();

                            $category = "";
                            foreach ($groupD as $groupKey) {
                                $category .= ($groupKey['category']) . "|";
                                $cats = explode("|", $category);
                            }

                            $catNo = count($cats) - 1;
                            $label = [];
                            $content = [];
                            $incidenceCode = "";

                            for ($i = 0; $i < $catNo; $i++) {

                                $query = new Query;
                                $query->from('Help')
                                        ->where('category like "' . $cats[$i] . '"');
                                $data = $query->all();
                                $label = array('label' => $cats[$i]);
                                $links = "";

                                foreach ($data as $key_value) {

                                    $incidenceCode = ($key_value['description']);
                                    $incidenceName = ($key_value['help_code']);
                                 //   $redirect = ($key_value['redirect']);
                                    
                                }
                                $content = array('content' => $incidenceCode);
                              //   $incidenceCode = ($key_value['help_code']);
                               $link[] = array_merge($label, $content);
                            }

                            echo Collapse::widget([
                                'items' => $link,
                            ]);
                            ?>
                        </tr>
                    </tbody>
                </table>
            </div>
   </div>
   <div class="col-lg-1">
       
   </div>
    <div class="col-lg-4">
     <h3> TYPES OF BETS</h3>
     <table class="table  borderless table-hover">
                    <tbody>
                        <tr>
                            <?php
                            $groupf = new Query;
                            $groupf->from('Help_sms')
                                    ->groupBy('category');
                            $groupG = $groupf->all();

                            $category1 = "";
                            foreach ($groupG as $groupKey1) {
                                $category1 .= ($groupKey1['category']) . "|";
                                $cats1 = explode("|", $category1);
                            }

                            $catNo1 = count($cats1) - 1;
                            $label1 = [];
                            $content1 = [];
                            $incidenceCode1 = "";

                            for ($i = 0; $i < $catNo1; $i++) {

                                $query = new Query;
                                $query->from('Help_sms')
                                        ->where('category like "' . $cats1[$i] . '"');
                                $data1 = $query->all();
                                $label1 = array('label' => $cats1[$i]);
                                $links1 = "";

                                foreach ($data1 as $key_value1) {

                                    $incidenceCode1 = ($key_value1['description']);
                                    $incidenceName1 = ($key_value1['help_code']);
                                 //   $redirect = ($key_value['redirect']);
                                    
                                }
                                $content1 = array('content' => $incidenceCode1);
                              //   $incidenceCode = ($key_value['help_code']);
                               $link1[] = array_merge($label1, $content1);
                            }

                            echo Collapse::widget([
                                'items' => $link1,
                            ]);
                            ?>
                        </tr>
                    </tbody>
                </table>
     
    
             </div>
             <div class="col-lg-1">
      </div>
  <div class="col-lg-2">
  <h3> ABBREVIATIONS</h3>
  <div class="dropdown">
    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Match Results <b class="caret"></b></a>
    <?php
        echo Dropdown::widget

        (['options'=>['class'=>'pull-right'], 
            'items' => [
                ['label' => '1=Home'],
                ['label' => 'X=Draw'],
                ['label' => '2=AWay'],
            ],
        ]);
    ?>
</div>
 <div class="dropdown">
    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Double Chance <b class="caret"></b></a>
    <?php
        echo Dropdown::widget

        (['options'=>['class'=>'pull-right'], 
            'items' => [
                ['label' => 'DC1X=Home or Draw'],
                ['label' => 'DC12=Home or Away'],
                ['label' => 'DCX2=Draw or Away'],
            ],
        ]);
    ?>
</div>
  
 
  <div class="dropdown">
    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Goal/No Goal <b class="caret"></b></a>
    <?php
        echo Dropdown::widget

        (['options'=>['class'=>'pull-right'], 
            'items' => [
                ['label' => 'GG=Both teams Score'],
                ['label' => 'NG=One or No teams'],
            ],
        ]);
    ?>
</div>
   
    
 
  <div class="dropdown">
    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Under/ Over<b class="caret"></b></a>
    <?php
        echo Dropdown::widget

        (['options'=>['class'=>'pull-right'], 
            'items' => [
                ['label' => 'UN=Under 1.5 total Goals less than 1'],
                ['label' => 'OV=Over 1.5 total Goals more than 2'],
                ['label' => 'UN=Under 2.5 total Goals less than 2'],
                ['label' => 'OV=Over 2.5 total Goals more than 3'],
            ],
        ]);
    ?>
</div>


  <div class="dropdown">
    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Odd/Even Total<b class="caret"></b></a>
    <?php
        echo Dropdown::widget

        (['options'=>['class'=>'pull-right'], 
            'items' => [
                ['label' => 'Odd = Odd total Goals eg: 1,3,5,7,9'],
                ['label' => 'Ev=Even total Goals eg:2,4,6,8..'],
               
            ],
        ]);
    ?>
</div>
   
 <div class="dropdown">
    <a href="#" data-toggle="dropdown" class="dropdown-toggle">MultiBet/ SingleBet<b class="caret"></b></a>
    <?php
        echo Dropdown::widget

        (['options'=>['class'=>'pull-right'], 
            'items' => [
                ['label' => 'MultiBet:Bet on many games & Markets in one'],
                ['label' => 'Singlebet: Bet on one game'],
               
            ],
        ]);
    ?>
</div>
  </div>

    </div>
</div>

       
