<?php

use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\DepDrop;
use kartik\helpers\Html;
use kartik\builder\Form;
use \yiister\gentelella\widgets\Panel;

$this->title = 'Reset Password';
/* @var $this yii\web\View */
/* @var $model app\models\ScheduleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?= \yiister\gentelella\widgets\FlashAlert::widget(['showHeader' => true]) ?>

<div class="row">
    <div class="col-md-12 col-xs-12">

<?php
Panel::begin();
$form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 
    'fullSpan' => 10, 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_SMALL], 'options' => ['enctype' => 'multipart/form-data']]);


Panel::begin(['header' => "Reset Password",]);
 echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
           
            'password' => [
                'type' => Form::INPUT_PASSWORD, 
                'options' => ['placeholder' => 'Password']
            ],
            'repeat_password' => [
                'type' => Form::INPUT_PASSWORD, 
                'options' => ['placeholder' => 'Repeat Password'],
                'hint' => 'To change the user password, enter the new password in both fields.'
            ],
            'actions' => [
                'type' => Form::INPUT_RAW,
                'value' => Html::submitButton('Reset Password', ['class' => 'btn btn-primary'])
            ],
       ],
    ]);   
Panel::end();
ActiveForm::end();
 Panel::end();
?>
</div>
</div>
