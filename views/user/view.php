<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \yiister\gentelella\widgets\Panel;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12 col-xs-12">

    <?php
        Panel::begin(['header' => "User Details",]);?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            // 'clientId',
            // 'roleId',
           
            [                      // the owner name of the model
                'label' => 'Role',
                'value' => $model->getRoleName(),
            ],
            'username',
            'email:email',
            'msisdn',
            //'password',
            //'salt',
            //'activationKey',
            //'status_id',
            [                      // the owner name of the model
                'label' => 'Status',
                'value' => $model->getStatusName(),
            ],
            'date_created',
            //'createdBy',
            //'dateModified',
            //'modifiedBy',
            [                      // the owner name of the model
                'label' => 'Created By',
                'value' => $model->getCreatedBy(),
            ],
        ],
    ]) ?>

 <?php Panel::end();
?>
</div>
</div>

